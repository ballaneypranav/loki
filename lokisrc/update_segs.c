/****************************************************************************
 *                                                                          *
 *     Loki - Programs for genetic analysis of complex traits using MCMC    *
 *                                                                          *
 *                       CNG  - Evry                                        *
 *                                                                          *
 *                       July 2002                                          *
 *                                                                          *
 * update_segs.c:                                                           *
 *                                                                          *
 * Interface routines between the L and M samplers                          *
 *                                                                          *
 * Copyright (C) Simon C. Heath 1997, 2000, 2002                            *
 * This is free software.  You can distribute it and/or modify it           *
 * under the terms of the Modified BSD license, see the file COPYING        *
 *                                                                          *
 ****************************************************************************/

#include <config.h>
#include <stdlib.h>
#include <stdio.h>

#include "utils.h"
#include "loki.h"
#include "loki_peel.h"
#include "seg_pen.h"
#include "gen_pen.h"
#include "update_segs.h"

#ifdef FUNC_NAME
#undef FUNC_NAME
#endif
#define FUNC_NAME "update_seg_probs"
void update_seg_probs(int fg,int fg1,int si)
{
	int i,k,comp;

	for(k=0;k<n_markers;k++) {
		if(fg&12) seg_init_freq(k);
		if(fg1&1) pass_founder_genes(k);
		for(comp=0;comp<n_comp;comp++) {
			marker[k].locus.lk_store[comp]=seg_pen(k,comp,&i,fg&~2,si);
			if(i) {
				(void)fprintf(stderr,"seg_pen returned error code %d for marker %s",(int)marker[k].locus.lk_store[comp],marker[k].name);
				if(marker[k].index) (void)fprintf(stderr,"(%d)",marker[k].index);
				(void)fprintf(stderr," (comp=%d, fg=%d, fg1=%d)\n",comp,fg,fg1);
				ABT_FUNC("Illegal segregation pattern\n");
			}
		}
		if(fg&4) seg_sample_freq(k);
		if(fg&8) seg_update_aff_freq(k);
		if(fg) marker[k].locus.flag|=LOCUS_SAMPLED;
	}
	for(k=0;k<n_tloci;k++) if(tlocus[k].locus.flag) {
 		if(fg&4) seg_init_freq(-1-k);
		if(fg1&2) pass_founder_genes(-1-k);
		pass_founder_genes(-1-k);
		for(comp=0;comp<n_comp;comp++) {
			tlocus[k].locus.lk_store[comp]=gen_pen(k,comp,&i,fg,si);
			if(i) {
				(void)fprintf(stderr,"%d %d %d %d\n",k,comp,i,(int)tlocus[k].locus.lk_store[comp]);
				ABT_FUNC("Illegal segregation pattern\n");
			}
		}
		if(fg) tlocus[k].locus.flag|=LOCUS_SAMPLED;
 		if(fg&4) seg_sample_freq(-1-k);
	}
}

void reprune_segs(void)
{
	int i,j,k,**seg,*ff;
	
	for(j=0;j<n_markers;j++) {
		seg=marker[j].locus.seg;
		ff=founder_flag[j];
		for(i=0;i<ped_size;i++) if(id_array[i].sire) {
			k=ff[i];
			if(k==2) seg[0][i]=seg[1][i]=-2;
			else if(k==1) seg[0][i]=seg[1][i]=-1;
		}
	}
}

