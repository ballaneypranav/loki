#define MSCAN_INDIVIDUAL .1
#define MSCAN_HS_FAMILY .4
#define MSCAN_FS_FAMILY .25
#define MSCAN_GP_FAMILY .25

extern void meiosis_scan(int,int);
extern double mscan_prob[];
#define seg_pen1(loc,loc1,a,b,c,d) ((loc)>=0?seg_pen(loc,a,b,c,d):(loc==loc1?gen_pen(-1-loc,a,b,c,d):seg_pen(loc,a,b,c,d)))
