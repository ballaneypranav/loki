/****************************************************************************
 *                                                                          *
 *     Loki - Programs for genetic analysis of complex traits using MCMC    *
 *                                                                          *
 *                      Simon Heath - MSKCC                                 *
 *                                                                          *
 *                          August 2000                                     *
 *                                                                          *
 * sample_cens.c:                                                           *
 *                                                                          *
 * Routines for sampling censored data                                      *
 *                                                                          *
 * Copyright (C) Simon C. Heath 1997, 2000, 2002                            *
 * This is free software.  You can distribute it and/or modify it           *
 * under the terms of the Modified BSD license, see the file COPYING        *
 *                                                                          *
 ****************************************************************************/

#include <math.h>
#include <stdio.h>

#include "utils.h"
#include "loki.h"
#include "ranlib.h"
#include "sample_cens.h"

/* Sample truncated (censored) data points */
void Sample_Censored(void)
{
	int i,j,type,idx,er;
	double sd,y,z;
	
	sd=sqrt(residual_var[0]);
	type=models[0].var.type;
	idx=models[0].var.var_index;
	if(type&ST_CONSTANT)	{
		for(i=0;i<ped_size;i++) if(id_array[i].res[0] && id_array[i].cens[0]) {
			y=id_array[i].res[0][0]-id_array[i].cens[0][0];
			z=sd*trunc_normal(y/sd,0.0,2,&er); /* Sample from normal left truncated at y */
			if(!er) {
				id_array[i].cens[0][0]=z-y;
				id_array[i].res[0][0]=z;
			}
		}
	} else for(i=0;i<ped_size;i++) if(id_array[i].res[0] && id_array[i].cens[0]) {
		for(j=0;j<id_array[i].n_rec;j++) {
			if(id_array[i].data1[j][idx].flag&2) {
				y=id_array[i].res[0][j]-id_array[i].cens[0][j];
				z=sd*trunc_normal(y/sd,0.0,2,&er);
				if(!er) {
					id_array[i].cens[0][j]=z-y;
					id_array[i].res[0][j]=z;
				}
			}
		}
	}
}

