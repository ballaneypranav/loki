extern void Output_Sample_Aff(int,double *,double *,int *,FILE *);
extern void OutputSample(FILE *,int);
extern void OutputFreqHeader(FILE *,loki_time *);
extern void OutputFreq(FILE *,int);
extern void OutputHeader(FILE *,int,loki_time *);
extern void Output_BV(FILE *);
extern void OutputQTLvect(FILE *,int);
extern void OutputQTLHead(FILE *);

extern int n_cov_columns,*ibd_mode;

