/****************************************************************************
 *                                                                          *
 *     Loki - Programs for genetic analysis of complex traits using MCMC    *
 *                                                                          *
 *             Simon Heath - Rockefeller University                         *
 *                                                                          *
 *                       November1997                                       *
 *                                                                          *
 * loki_tlmoves.h:                                                          *
 *                                                                          *
 * Defines for move probabilities                                           *
 *                                                                          *
 ****************************************************************************/

#define BIGMOVE_PROB .25
#define SMALLMOVE_P 0.5

#define BETA_A 5.75
#define BETA_B 1.25

#define BIRTH_STEP 0.5
#define DEATH_STEP 0.5

#define PROP_RATIO 0.1

extern void Sample_LinkageGroup(const int,struct peel_mem *,int);
extern void Sample_TL_Position(const int,struct peel_mem *,int);
extern void TL_Birth_Death(struct peel_mem *,int);
extern void TL_Alloc(void),TL_Free(void);
extern double calc_tl_like(const int,int *,const int,struct peel_mem *,int);
extern int get_tl_position(double *);
#ifdef DEBUG
extern void Flip_TL_Alleles(const int,int,struct peel_mem *);
#else
extern void Flip_TL_Alleles(const int);
#endif
