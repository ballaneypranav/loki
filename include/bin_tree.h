#ifndef _BIN_TREE_H_
#define _BIN_TREE_H_

#ifndef BIN_TREE_H
#define BIN_TREE_H 1

struct bin_node {
	struct bin_node *left,*right;
	void *data;
	int balance;
};

struct bin_node *rotate_left(struct bin_node *);
struct bin_node *rotate_right(struct bin_node *);
void free_bin_tree(struct bin_node *,void (*)(void *));

#endif

#endif
